# LeapMotion

This is a Processing library for using the LeapMotion controller.

The project is a continuation from [LeapMotionP5](https://github.com/Neurogami/LeapMotionP5), updated for use with the latest version of the LeapMotion SDK, as well as to work with Processing version 3.

At the moment the library is Windows-only.

At some point efforts will be made to get it running on OSX and Ubuntu.

Documentation will be updated once a more thorough review is done.

The compiled library has been superficially tested with Processing 3.3.7 and LeapMotion SDK 3.2.1+45911


